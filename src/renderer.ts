import './index.css';
import { Task } from './model/todos/Task';

onLoad();


async function onLoad() {
    const tasks = await window.electronApi.getTodos();
    const main = document.querySelector('main');

    main.appendChild(renderTasks(tasks));
}

function renderTasks(tasks: Task[]): HTMLElement {
    const ul = document.createElement('ul');

    for (const task of tasks) {
        const li = document.createElement('li');
        li.textContent = task.label;
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.checked = task.done;
        li.appendChild(checkbox);
        ul.appendChild(li);
    }

    return ul;
}