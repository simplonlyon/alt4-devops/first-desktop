import { Task } from "./model/todos/Task"

export interface IElectronAPI {
  doCoucou: () => Promise<string>
  getTodos: () => Promise<Task[]>
}
declare global {
  interface Window {
    electronApi: IElectronAPI
  }
}