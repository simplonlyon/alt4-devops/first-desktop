import { contextBridge, ipcRenderer } from "electron";


contextBridge.exposeInMainWorld('electronApi', {
    doCoucou: () => ipcRenderer.invoke('exemple', 'coucou'),
    getTodos: () => ipcRenderer.invoke('get-todos')
});